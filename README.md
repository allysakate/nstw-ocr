#NSTW
Run lidetect.py (A&B)
Run ocr1.py     (plate detection - image processing only)

A. License Plate Detection

based on Tensorflow Object Detection API: 

- https://github.com/tensorflow/models/tree/master/research/object_detection

1. Re-train the model to detect license plate
	- https://pythonprogramming.net/introduction-use-tensorflow-object-detection-api-tutorial/

B. License Plate Recognition

based on: 

- https://github.com/zubairahmed-ai/Number-Plate-Recognition

- https://github.com/Deevoluation/ALPR (use anpr/train_detect.py for training chars)

1. Re-train the model for characters
	- created 2 models: (1)letter, (2)numbers

2. Modified ocr/DetectChars.py
	- at recognizeCharsInPlate: 1st to 3rd char -> use letter-model and 4th to 6th/7th char -> use number-model

3. Modified lidetect.py/ocr.py
	- cropped the detected plate
	- input cropped plate to ocr/Main.py
	- Main.py outputs stringChars
	- make condition statement for NumberCoding
	- use drawRedRectangleAroundPlate fxn to put bounding box w/ the chars & coding day
	>> ocr.py
	- save date,time, coding, OCR, location in NSTW-CODING.csv
	- if OCR is less than 6/7 chars it will show the previous OCR with 6/7 chars
	