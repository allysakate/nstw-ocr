import numpy as np
import os
import six.moves.urllib as urllib
import sys
import tarfile
import tensorflow as tf
import zipfile
import time

from collections import defaultdict
from io import StringIO
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
from PIL import Image

import cv2
sys.path.append("..")
from api import label_map_util
from api import visualization_utils as vis_util

from ocr import Main

PATH_TO_CKPT = 'api/frozen_inference_graph.pb'
PATH_TO_LABELS = 'api/object-detection.pbtxt'
NUM_CLASSES = 1

label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
category_index = label_map_util.create_category_index(categories)

detection_graph = tf.Graph()
with detection_graph.as_default():
    od_graph_def = tf.GraphDef()
    with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
        serialized_graph = fid.read()
        od_graph_def.ParseFromString(serialized_graph)
        tf.import_graph_def(od_graph_def, name='')

    sess = tf.Session(graph=detection_graph)

image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')
num_detections = detection_graph.get_tensor_by_name('num_detections:0')

#
def drawRedRectangleAroundPlate(frame, ymin, xmin, ymax, xmax, displayMsg):  
    pts = np.array([[xmin,ymin],[xmax,ymin],[xmax,ymax],[xmin,ymax]], np.int32)
    pts = pts.reshape((-1,1,2))
    cv2.polylines(frame,[pts],True,(0,255,0),2)    

    yLefta = ymax + 30
    yLeft1 = ymax + 40
    yRight1 = ymax + 40
 
    #display codeDay
    pts = np.array([[xmin,ymax],[xmax,ymax],[xmax,yRight1],[xmin,yLeft1]],np.int32)
    pts = pts.reshape((-1,1,2))
    cv2.fillPoly(frame,[pts],(0,255,0),8)
    font = cv2.FONT_HERSHEY_SIMPLEX
    cv2.putText(frame,displayMsg,(xmin,yLefta), font, 0.8,(0,0,0),2,cv2.LINE_AA)
    
    #darken area with no plate
    plateImg = frame[ymin:yLeft1,xmin:xmax]
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV) #convert it to hsv
    hsv[...,2] = hsv[...,2]*0.2 # 0.1-0.9, 0.1 darkest
    image = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)
    image[ymin:yLeft1,xmin:xmax] = plateImg
    return image 

# Open video file
video = cv2.VideoCapture('plates.mp4')
#video = cv2.VideoCapture(0)
width = video.get(cv2.CAP_PROP_FRAME_WIDTH)
height = video.get(cv2.CAP_PROP_FRAME_HEIGHT)
area = int(height * width)
while(video.isOpened()):
    start_time = time.process_time()
    ret, frame = video.read()
    if ret == True:
        frame_expanded = np.expand_dims(frame, axis=0)
    else:
        break
    (boxes, scores, classes, num) = sess.run(
        [detection_boxes, detection_scores, detection_classes, num_detections],
        feed_dict={image_tensor: frame_expanded})

    image, xmin, xmax, ymin, ymax  = vis_util.visualize_boxes_and_labels_on_image_array(
        frame,
        np.squeeze(boxes),
        np.squeeze(classes).astype(np.int32),
        np.squeeze(scores),
        category_index,
        use_normalized_coordinates=True,
        line_thickness=1,
        min_score_thresh=0.90)
    ymin = int(ymin * height)
    xmin = int(xmin * width)
    ymax = int(ymax * height)
    xmax = int(xmax * width)
    width1 = xmax - xmin
    height1 = ymax - ymin
    area1 = height1 * width1

    if xmax == 0 or area1 > 0.7*area:
        image1 = frame
    else:
        cropped = image[ymin:ymax, xmin:xmax]
        plateChars, response = ocr.main(cropped)
        lastNum = ''
        codeDay = '  '
        validChars = '  '
        if response == 1:
            image1 = frame
        if len(plateChars) in [6,7]:
            validChars = plateChars
            lastNum = plateChars[-1]
            lastNum = int(lastNum)
            if   lastNum in [1,2]:
                codeDay = '(Coding: Monday)'
            elif lastNum in [3,4]:
                codeDay = '(Coding: Tuesday)'
            elif lastNum in [5,6]:
                codeDay = '(Coding: Wednesday)'
            elif lastNum in [7,8]:
                codeDay = '(Coding: Thursday)'
            elif lastNum in [9,0]:
                codeDay = '(Coding: Friday)'
            else:
                codeDay = ' '
            displayMsg = '  ' + validChars + '  ' + codeDay
            image1 = drawRedRectangleAroundPlate(frame, ymin, xmin, ymax, xmax, displayMsg)   
    cv2.imshow('ocr',image1)
    k = cv2.waitKey(1)
    if k==27:    # Esc key to stop
      break
    
cv2.destroyAllWindows()
video.release()
