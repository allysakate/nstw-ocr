from keras.models import Sequential # To initialise the nn as a sequence of layers
from keras.layers import Convolution2D # To make the convolution layer for 2D images
from keras.layers import MaxPooling2D # 
from keras.layers import Flatten
from keras.layers import Dense
from keras.layers import Dropout
from keras.callbacks import CSVLogger
from keras.optimizers import RMSprop
from keras.models import model_from_json

import cv2 
import numpy as np

# Initialising the CNN
classifier = Sequential()

# Step 1 - Convolution
classifier.add(Convolution2D(32,(3,3),input_shape = (64,64,3), activation = 'relu'))
# Step 2 - Pooling
classifier.add(MaxPooling2D(pool_size = (2,2)))

# Step 1 - Convolution
classifier.add(Convolution2D(32,(3,3),input_shape = (64,64,3), activation = 'relu'))
# Step 2 - Pooling
classifier.add(MaxPooling2D(pool_size = (2,2)))
# Step 3 - Flattening
classifier.add(Flatten())

classifier.add(Dense(128, activation = 'relu'))
classifier.add(Dropout((0.07)))
classifier.add(Dense(10, activation = 'softmax')) #change Letter 26, Number 10

csv=CSVLogger("epochs2.log")

# Compiling the CNN
classifier.compile(optimizer = RMSprop(lr=0.001,rho=0.9,epsilon=1e-08,decay=0.005), loss = 'categorical_crossentropy', metrics = ['accuracy'])

from keras.preprocessing.image import ImageDataGenerator

train_datagen = ImageDataGenerator(rescale=1./255)

test_datagen = ImageDataGenerator(rescale=1./255)

train_set = train_datagen.flow_from_directory('TrainNum',target_size=(64, 64),batch_size=32,class_mode='categorical') #change Train folder

test_set = test_datagen.flow_from_directory('TestNum',target_size=(64, 64),batch_size=32,class_mode='categorical') #change Test folder

classifier.fit_generator(train_set,steps_per_epoch=300,epochs=5,validation_data=test_set,validation_steps=150,callbacks=[csv]) #change Letter:780,390 ; Number: 300,150

#save classifier
classifier_json = classifier.to_json()
with open("number-reg.json", "w") as json_file:
    json_file.write(classifier_json)
# serialize weights to HDF5
classifier.save_weights("number-reg.h5")
print("Saved classifier!")
 
# load json and create classifier
json_file = open('number-reg.json', 'r')
loaded_classifier_json = json_file.read()
json_file.close()
loaded_classifier = model_from_json(loaded_classifier_json)
# load weights into new classifier
loaded_classifier.load_weights("number-reg.h5")
print("Loaded classifier!")

loaded_classifier.compile(optimizer = RMSprop(lr=0.001,rho=0.9,epsilon=1e-08,decay=0.005), loss = 'categorical_crossentropy', metrics = ['accuracy'])

img=cv2.imread('temp1.jpg')
img=cv2.resize(img,(64,64))
img=np.reshape(img,[1,64,64,3])
new_classes=loaded_classifier.predict_classes(img)
print('new', new_classes)