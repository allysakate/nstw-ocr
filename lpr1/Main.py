import glob
import cv2
import numpy as np
import os
import time
from lpr import DetectChars
from lpr import DetectPlates
from lpr import PossiblePlate

def recognize(image):
    imgOriginalScene = image
    image1 = image
    #imgOriginalScene = cv2.resize(imgOriginalScene, (0, 0), fx = 1.4, fy = 1.4,interpolation=cv2.INTER_LINEAR)    
    if imgOriginalScene is None:                            
        print("\nerror: image not read from file \n\n")      
        os.system("pause")                                  
        return                                              

    listOfPossiblePlates = DetectPlates.detectPlatesInScene(imgOriginalScene)           
                                                                                        
    listOfPossiblePlates = DetectChars.detectCharsInPlates(listOfPossiblePlates)        

    plateChars = ''
    lastNum    = ''
    codeDay    = '  '
    validChars = '  '
    ret = 0
    listOfPossiblePlates.sort(key = lambda possiblePlate: len(possiblePlate.strChars), reverse = True)
    black = np.zeros((480,640,3),np.uint8)
    
    if len(listOfPossiblePlates) == 0:
        ret = 0
        image1 = imgOriginalScene
    else:   
        licPlate = listOfPossiblePlates[0]                                                   
        plateChars = listOfPossiblePlates[0].strChars
        if len(plateChars) in [6,7]:
            validChars = plateChars
            lastNum = plateChars[-1]
            lastNum = int(lastNum)
            if   lastNum in [1,2]:
                codeDay = '(Monday)'
                ret = 1
            elif lastNum in [3,4]:
                codeDay = '(Tuesday)'
                ret = 2
            elif lastNum in [5,6]:
                codeDay = '(Wednesday)'
                ret = 3
            elif lastNum in [7,8]:
                codeDay = '(Thursday)'
                ret = 4
            elif lastNum in [9,0]:
                codeDay = '(Friday)'
                ret = 5
            else:
                codeDay = ' '
            self.saveMsg    = '  '
            displayMsg = ' ' + validChars + ' ' + codeDay
            self.saveMsg    = displayMsg
        else:
            ret = 9
            displayMsg = self.saveMsg
        image1, black, ret = drawRedRectangleAroundPlate(image, black, licPlate, displayMsg, ret)       
    return image1, black, plateChars, ret

def drawRedRectangleAroundPlate(image, black, licPlate, displayMsg, ret):
    p2fRectPoints = cv2.boxPoints(licPlate.rrLocationOfPlateInScene)            
    xmin = int(p2fRectPoints[1][0])
    ymin = int(p2fRectPoints[1][1])
    xmax = int(p2fRectPoints[3][0])
    ymax = int(p2fRectPoints[3][1])
    width  = xmax - xmin
    height = ymax - ymin
    area   = width * height
    if height < 50 and area <= 10000:
        ret = 9
        return image, black, ret
    else:  
        pts = np.array([[xmin,ymin],[xmax,ymin],[xmax,ymax],[xmin,ymax]], np.int32)
        pts = pts.reshape((-1,1,2))
        cv2.polylines(image,[pts],True,(0,255,0),2)    

        widthA  = xmax - xmin
        fontScale = widthA / 1000
        if fontScale > 0.5:
            add1  = 25
            add2  = 35
            scale = 0.7
        else:
            add1  = 15
            add2  = 25
            scale = 0.5

        yLefta  = ymax + add1
        yLeft1  = ymax + add2
        yRight1 = ymax + add2

        pts = np.array([[xmin,ymax],[xmax,ymax],[xmax,yRight1],[xmin,yLeft1]],np.int32)
        pts = pts.reshape((-1,1,2))
        cv2.fillPoly(image,[pts],(0,255,0),8)
        font = cv2.FONT_HERSHEY_SIMPLEX
        cv2.putText(image,displayMsg,(xmin,yLefta), font, scale,(0,0,0),1,cv2.LINE_AA)
    
        plateImg = image[ymin:yLeft1,xmin:xmax]
        imgcrop  = plateImg.copy()

        hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
        hsv[...,2] = hsv[...,2]*0.2 # 0.1-0.9, 0.1 darkest
        image = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)
        image[ymin:yLeft1,xmin:xmax] = plateImg

        #cropped = cv2.resize(imgcrop,(585,210))
        #black[135:345,28:613] = cropped

    return image, black, ret