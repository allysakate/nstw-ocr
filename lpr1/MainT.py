import glob
import cv2
import numpy as np
import os
import time
from lpr import DetectCharsT as DetectChars
from lpr import DetectPlatesT as DetectPlates
from lpr import PossiblePlate

SCALAR_BLACK = (0.0, 0.0, 0.0)
SCALAR_WHITE = (255.0, 255.0, 255.0)
SCALAR_YELLOW = (0.0, 255.0, 255.0)
SCALAR_GREEN = (0.0, 255.0, 0.0)
SCALAR_RED = (0.0, 0.0, 255.0)

def main(image,count):
    imgOriginalScene = image
    image1 = image
    #imgOriginalScene = cv2.resize(imgOriginalScene, (0, 0), fx = 1.4, fy = 1.4,interpolation=cv2.INTER_LINEAR)    
    if imgOriginalScene is None:                            
        print("\nerror: image not read from file \n\n")      
        os.system("pause")                                  
        return                                              

    listOfPossiblePlates = DetectPlates.detectPlatesInScene(imgOriginalScene)           
                                                                                        
    listOfPossiblePlates, count = DetectChars.detectCharsInPlates(listOfPossiblePlates,count)        

    plateChars = ''
    lastNum    = ''
    codeDay    = '  '
    validChars = '  '
    listOfPossiblePlates.sort(key = lambda possiblePlate: len(possiblePlate.strChars), reverse = True)
    
    response = 0
    ret = 0
    if len(listOfPossiblePlates) == 0:
        ret = 0
        image1 = imgOriginalScene
    else:   
        licPlate = listOfPossiblePlates[0]                                                   
        plateChars = listOfPossiblePlates[0].strChars
        if len(plateChars) in [6,7]:
            validChars = plateChars
            lastNum = plateChars[-1]
            lastNum = int(lastNum)
            if   lastNum in [1,2]:
                codeDay = '(Coding: Monday)'

            elif lastNum in [3,4]:
                codeDay = '(Coding: Tuesday)'
            elif lastNum in [5,6]:
                codeDay = '(Coding: Wednesday)'
            elif lastNum in [7,8]:
                codeDay = '(Coding: Thursday)'
            elif lastNum in [9,0]:
                codeDay = '(Coding: Friday)'
            else:
                codeDay = ' '
            displayMsg = ' ' + validChars + ' ' + codeDay
            image1, ret = drawRedRectangleAroundPlate(image, licPlate, displayMsg, count)        
    return image1, plateChars, ret, count

def drawRedRectangleAroundPlate(image, licPlate, displayMsg, count):
    ret = 9
    p2fRectPoints = cv2.boxPoints(licPlate.rrLocationOfPlateInScene)            
    xmin = int(p2fRectPoints[1][0])
    ymin = int(p2fRectPoints[1][1])
    xmax = int(p2fRectPoints[3][0])
    ymax = int(p2fRectPoints[3][1])
    width = (xmax-xmin)
    height = (ymax-ymin)
    area = width * height
    print(str(count) + '-' + licPlate.strChars + '; area: ' + str(area))
    
    if height < 30 and area <= 10000:
        ret = 0
        return image, ret

    pts = np.array([[xmin,ymin],[xmax,ymin],[xmax,ymax],[xmin,ymax]], np.int32)
    pts = pts.reshape((-1,1,2))
    cv2.polylines(image,[pts],True,(0,255,0),2)    

    widthA  = xmax - xmin
    fontScale = widthA / 1000
    if fontScale > 0.5:
        add1  = 30
        add2  = 40
        scale = 0.8
    else:
        add1  = 20
        add2  = 30
        scale = 0.6

    yLefta  = ymax + add1
    yLeft1  = ymax + add2
    yRight1 = ymax + add2

    pts = np.array([[xmin,ymax],[xmax,ymax],[xmax,yRight1],[xmin,yLeft1]],np.int32)
    pts = pts.reshape((-1,1,2))
    cv2.fillPoly(image,[pts],(0,255,0),8)
    font = cv2.FONT_HERSHEY_SIMPLEX
    cv2.putText(image,displayMsg,(xmin,yLefta), font, scale,(0,0,0),2,cv2.LINE_AA)
    
    plateImg = image[ymin:yLeft1,xmin:xmax]
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    hsv[...,2] = hsv[...,2]*0.2 # 0.1-0.9, 0.1 darkest
    image = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)
    image[ymin:yLeft1,xmin:xmax] = plateImg
    
    return image, ret