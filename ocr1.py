import glob
import cv2
import numpy as np
import os
import time
import cv2
import datetime
import csv

from lpr import Main
from lpr import DetectChars

cons = 5 #frame/s
window_name = "NSTW - Number Coding Live Demo"
video = cv2.VideoCapture(1)
if not video.isOpened():
    print("Error: Could not open video.")
    exit()

#cv2.namedWindow(window_name, cv2.WND_PROP_FULLSCREEN)
#cv2.setWindowProperty(window_name, cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
framectr = 0
count = 1
platectr = 1
while(video.isOpened()):
    start_time = time.process_time()
    ret2, frame = video.read()
    if ret2 == False:
        break
    frame = cv2.resize(frame,(640,480))
    imgframe = frame.copy()
    framectr = framectr + 1
    if framectr % cons == 0:
        lastNum    = ''
        codeDay    = '  '
        validChars = '  '
        plateChars = '--'
        plate, black, plateChars, ret = Main.recognize(frame)

        # This one when the last plate is detected correctly.
        # The numbers states the last number of the plate
        if ret in [1,2,3,4,5]:
            if ret == 1:
                cday = 'Monday'
            elif ret == 2:
                cday = 'Tuesday'
            elif ret == 3:
                cday = 'Wednesday'
            elif ret == 4:
                cday = 'Thursday'
            else:
                cday = 'Friday'

            outname   = '/home/aicatch/nstw-ocr/lpr/coding/' + str(ret) + '-' + cday + '/' + str(platectr) + '-' + str(plateChars) + '.jpg'
            cv2.imwrite(outname, plate)
            platectr = platectr + 1
            currentDT = datetime.datetime.now()
            with open('NSTW-CODING.csv', 'a', newline='') as csvfile:
                fieldnames = ['TIME','DATE','OCR','CODING','FILE LOCATION']
                writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
                time1 = currentDT.strftime("%I:%M:%S %p")
                date1 = currentDT.strftime("%Y/%m/%d")
                writer.writerow({'TIME': time1, 'DATE':date1, 'OCR': plateChars, 'CODING':cday, 'FILE LOCATION':outname})
        
        # Stack the "black" and plate view then Display
        hstack1 = np.hstack((black,plate))
        cv2.imshow(window_name,hstack1)
        

        # Compute Processing time
        show_time = time.process_time() - start_time     
        write_time = time.process_time() - start_time
        print('show:' + str(show_time))
        print('write' + str(write_time))
        k = cv2.waitKey(1)
        if k==27:    # Esc key to stop
            print("Exit requested.")
            break
    
cv2.destroyAllWindows()
video.release()