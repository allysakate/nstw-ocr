
import glob
import cv2
import numpy as np
import os
import time
from ocr import DetectChars
from ocr import DetectPlates
from ocr import PossiblePlate

SCALAR_BLACK = (0.0, 0.0, 0.0)
SCALAR_WHITE = (255.0, 255.0, 255.0)
SCALAR_YELLOW = (0.0, 255.0, 255.0)
SCALAR_GREEN = (0.0, 255.0, 0.0)
SCALAR_RED = (0.0, 0.0, 255.0)

def main(image):
    CnnClassifier = DetectChars.loadCNNClassifier() 
    if CnnClassifier == False:                               
        print("\nerror: CNN traning was not successful\n")
        return                                            

    imgOriginalScene = image
    #imgOriginalScene = cv2.resize(imgOriginalScene, (0, 0), fx = 1.4, fy = 1.4,interpolation=cv2.INTER_LINEAR)    
    if imgOriginalScene is None:                            
        print("\nerror: image not read from file \n\n")      
        os.system("pause")                                  
        return                                              

    listOfPossiblePlates = DetectPlates.detectPlatesInScene(imgOriginalScene)           
                                                                                        
    listOfPossiblePlates = DetectChars.detectCharsInPlates(listOfPossiblePlates)        

    if len(listOfPossiblePlates) == 0:
        response = 0
        return response,imgOriginalScene
    else:                                                      
        listOfPossiblePlates.sort(key = lambda possiblePlate: len(possiblePlate.strChars), reverse = True)
        licPlate = listOfPossiblePlates[0]

    return licPlate.strChars, response